package com.example.plugin;

import org.apache.cordova.*;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.io.DataOutputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class execsu extends CordovaPlugin {

    @Override
    public boolean execute(String action, JSONArray data, CallbackContext callbackContext) throws JSONException {

        if (action.equals("execassu")) {
            Process p; 
            String command = data.getString(0);
            try { 
                 // Preform su to get root privledges
                     p = Runtime.getRuntime().exec("su"); 
                        
                 // Attempt to write a file to a root-only 
                 DataOutputStream os = new DataOutputStream(p.getOutputStream());
                 BufferedReader bf = new BufferedReader(new InputStreamReader(p.getInputStream())); 
                 os.writeBytes(command+"\n");
                 os.writeBytes("exit\n");
                 String temp = "";
                 JSONObject outputJSON = new JSONObject();
                 int cnt = 1;
                 while((temp = bf.readLine()) != null)
                 {
                    System.out.println(temp);
                    outputJSON.put("line"+cnt,temp);
                    cnt++;
                 }
                                    
                 os.flush(); 
                 try { 
                      p.waitFor(); 
                      if (p.exitValue() != 255) { 
                          // TODO Code to run on success
                          callbackContext.success(outputJSON);
                          return true;  
                      } 
                      else { 
                          // TODO Code to run on unsuccessful
                          callbackContext.error("not root");
                          return false;
                      } 
                 } catch (InterruptedException e) { 
                      // TODO Code to run in interrupted exception
                      callbackContext.error("not root");
                      return false;
                 } 
            } catch (IOException e) { 
                 // TODO Code to run in input/output exception
                 callbackContext.error("not root");
                 return false;
            }

        } else {
            callbackContext.error("something wrong!");            
            return false;

        }
    }
}
