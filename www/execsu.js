/*global cordova, module*/

module.exports = {
    run: function (command, successCallback, errorCallback) {
        cordova.exec(successCallback, errorCallback, "execsu", "execassu", [command]);
    }
};
